import React,{useState,useEffect} from 'react';
import ItemList from './components/ItemList';
import axios from "axios";


const Home = ()=>{
    const [data,setData] = useState();
    useEffect(()=>{
        const url = "http://localhost:8003/ItemList/"
        axios.get(url).then(response => setData(response.data))
    },[])
    


    return(
        <>
        <ItemList data={data}/>
       
      </>
    )
}


export default Home;