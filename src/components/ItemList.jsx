import React from "react";
import {useNavigate} from 'react-router-dom'
import axios from 'axios';
import Button from '@mui/material/Button';
const ItemList =({data})=>{
    const navigate = useNavigate();
    const DeleteUser = (id) =>{
        const url = `http://localhost:8003/ItemList/${id}`
        try{
      axios.delete(url).then((response)=> {console.log(response);alert('Your Data has been deleted successfully'); window.location.reload()})
        }catch(err){
            alert(err)
        }
    } 
    return(
        <>
        <h1>User Data</h1>
        <div className="button">
        <Button onClick={()=> navigate('/component/createNew')} >Add User +</Button>
        </div>
        <table className="table">
            <thead>
                <tr>
                <th>
                    id
                </th>
                <th>
                    Name
                </th>
                <th>
                    Color
                </th>
                <th>
                    Age
                </th>
                
                </tr>
            </thead>
            <tbody>
            {data && data.map((curElem,Index)=>{
            return(
                <tr key={Index}>
                    <td>
                        {curElem.id}
                    </td>
                    <td>
                        {curElem.name}
                    </td>
                    <td>
                        {curElem.color}
                    </td>
                    <td>
                        {curElem.age}
                    </td>
                    <td>
                        <Button onClick={()=> navigate('/component/createNew',{state : {user:curElem}})}>Edit</Button>
                    </td>
                    <td>
                        <Button onClick={()=>DeleteUser(curElem.id)}>Delete</Button>
                    </td>
                </tr>
            )
        })} 
            </tbody>
        </table>
      
        </>
    )
}

export default ItemList;