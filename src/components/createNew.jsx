import axios from 'axios';
import React, { useEffect, useState } from 'react'
import {  useLocation, useNavigate } from 'react-router-dom';
import Button from '@mui/material/Button'
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';;
const NewUser = ()=>{
    const navigate=useNavigate();
    const params = useLocation();
    const [id,setId] = useState();
    const [data,setData] =useState({
        name: "",
        color: "",
        age:""
    })
    useEffect(()=>{
        if(params.state){
           setData({
            ...data,
            id:params.state.user.id,
            name: params.state.user.name,
        color: params.state.user.color,
        age:params.state.user.age
           });
           setId(params.state.user.id)

        }
    },[])
      const changeInputValue = (e) => {
        const { name, value } = e.target;
        setData({ ...data, [name]: value });
      };
      const UpdateUser = ()=>{
        const url = `http://localhost:8003/ItemList/${id}`
        try{
        axios.put(url,data).then((response)=> {console.log(response);navigate('/')})
    }catch(err){
        alert(err)
    }
      }
      const CreateUser = ()=>{
        const url = `http://localhost:8003/ItemList/`
        try{
        axios.post(url,data).then((response)=> {console.log(response); navigate('/')})
        }catch(err){
            alert(err)
        }
    }
    return(
        <>
        <h1>{params.state ? "Update User":"New User"}</h1>
        {/* <form>
        <input type="text" placeholder='name' name="name" value={data.name} onChange={changeInputValue} />
        <input type="text" placeholder='color' name="color" value={data.color} onChange={changeInputValue}/>
        <input type="tel" placeholder='age'  name ="age" value={data.age} onChange={changeInputValue}/>
        </form> */}
        <Box
      component="form"
      sx={{
          '& > :not(style)': { m: 1, width: '25ch' },
        }}
        noValidate
        autoComplete="off"
        style={{width:'70%',marginLeft:'auto',marginRight:'auto',marginTop:'4rem'}}
        >
      <TextField id="outlined-basic" label="Name" variant="outlined" name="name" value={data.name} onChange={changeInputValue} />
      <TextField id="outlined-basic" label="Color" variant="outlined" name="color" value={data.color} onChange={changeInputValue} />
      <TextField id="outlined-basic" label="Age" variant="outlined" name ="age" value={data.age} onChange={changeInputValue}/><br/>
        <div className='button'>
        {params.state ? <Button onClick={()=> UpdateUser()}>Update</Button> : <Button onClick={()=> CreateUser()}>Create</Button>}
        </div>
    </Box>
        </>
    )
}


export default NewUser;