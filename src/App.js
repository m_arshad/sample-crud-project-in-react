import React from 'react'
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
  Link,
} from "react-router-dom";

import Home from './home';
import NewUser from './components/createNew';
function App() {
  
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/component/createNew" element={<NewUser />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
